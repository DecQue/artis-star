/**
 * @file kernel/dtss/Coordinator.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2019 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DTSS_POLICY
#define DTSS_POLICY

#include <artis-star/common/Bag.hpp>

#include <cassert>

namespace artis {
namespace dtss {

class Policy
{
public:
  Policy() = default;

  virtual ~Policy() = default;

  const common::Bag<common::DoubleTime> &bag() const
  { return _bag; }

  void clear()
  { _bag.clear(); }

  void push(const common::ExternalEvent<common::DoubleTime> &event)
  { _bag.push_back(event); }

private:
  common::Bag<common::DoubleTime> _bag;
};

class AllEventsPolicy : public Policy
{
public:
  AllEventsPolicy() = default;

  virtual ~AllEventsPolicy() = default;

  virtual void operator()(
      typename common::DoubleTime::type /* t */,
      const common::ExternalEvent<common::DoubleTime> &event,
      typename common::DoubleTime::type /* tl */,
      typename common::DoubleTime::type /* tn */)
  {
    push(event);
  }
};

class LastBagPolicy : public Policy
{
public:
  LastBagPolicy() = default;

  virtual ~LastBagPolicy() = default;

  virtual void operator()(
      typename common::DoubleTime::type /* t */,
      const common::ExternalEvent<common::DoubleTime> &event,
      typename common::DoubleTime::type /* tl */,
      typename common::DoubleTime::type /* tn */)
  {
    clear();
    push(event);
  }
};

class IgnorePolicy : public Policy
{
public:
  IgnorePolicy() = default;
  virtual ~IgnorePolicy() = default;

  virtual void operator()(
      typename common::DoubleTime::type /* t */,
      const common::ExternalEvent<common::DoubleTime> & /* event */,
      typename common::DoubleTime::type /* tl */,
      typename common::DoubleTime::type /* tn */)
  {}
};

}
}

#endif

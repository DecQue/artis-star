INCLUDE_DIRECTORIES(
        ${ARTIS_BINARY_DIR}/src
        ${ARTIS_SOURCE_DIR}/src
        ${Boost_INCLUDE_DIRS})

LINK_DIRECTORIES(
        ${Boost_LIBRARY_DIRS})

SET(PDEVS_MPI_HPP Coordinator.hpp GraphManager.hpp LogicalProcessor.hpp
        ModelProxy.hpp)

INSTALL(FILES ${PDEVS_MPI_HPP} DESTINATION
        ${ARTIS_INCLUDE_DIRS}/kernel/pdevs/mpi)